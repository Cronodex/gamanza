--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: gamanza; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA gamanza;


ALTER SCHEMA gamanza OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: transaction_source; Type: TYPE; Schema: gamanza; Owner: ubuntu
--

CREATE TYPE gamanza.transaction_source AS ENUM (
    'web',
    'api'
);


ALTER TYPE gamanza.transaction_source OWNER TO ubuntu;

--
-- Name: transaction_type; Type: TYPE; Schema: gamanza; Owner: ubuntu
--

CREATE TYPE gamanza.transaction_type AS ENUM (
    'deposit',
    'withdrawal',
    'transfer'
);


ALTER TYPE gamanza.transaction_type OWNER TO ubuntu;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: transactions; Type: TABLE; Schema: gamanza; Owner: ubuntu
--

CREATE TABLE gamanza.transactions (
    transactionid integer NOT NULL,
    "timestamp" timestamp with time zone DEFAULT now() NOT NULL,
    source gamanza.transaction_source NOT NULL,
    amount integer NOT NULL,
    type gamanza.transaction_type NOT NULL
);


ALTER TABLE gamanza.transactions OWNER TO ubuntu;

--
-- Name: transactions_transactionid_seq; Type: SEQUENCE; Schema: gamanza; Owner: ubuntu
--

CREATE SEQUENCE gamanza.transactions_transactionid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gamanza.transactions_transactionid_seq OWNER TO ubuntu;

--
-- Name: transactions_transactionid_seq; Type: SEQUENCE OWNED BY; Schema: gamanza; Owner: ubuntu
--

ALTER SEQUENCE gamanza.transactions_transactionid_seq OWNED BY gamanza.transactions.transactionid;


--
-- Name: transfers; Type: TABLE; Schema: gamanza; Owner: ubuntu
--

CREATE TABLE gamanza.transfers (
    transferid integer NOT NULL,
    transactionid integer NOT NULL,
    senderid integer NOT NULL,
    receiverid integer NOT NULL
);


ALTER TABLE gamanza.transfers OWNER TO ubuntu;

--
-- Name: transfers_transferid_seq; Type: SEQUENCE; Schema: gamanza; Owner: ubuntu
--

CREATE SEQUENCE gamanza.transfers_transferid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gamanza.transfers_transferid_seq OWNER TO ubuntu;

--
-- Name: transfers_transferid_seq; Type: SEQUENCE OWNED BY; Schema: gamanza; Owner: ubuntu
--

ALTER SEQUENCE gamanza.transfers_transferid_seq OWNED BY gamanza.transfers.transferid;


--
-- Name: user_transaction; Type: TABLE; Schema: gamanza; Owner: ubuntu
--

CREATE TABLE gamanza.user_transaction (
    userid integer NOT NULL,
    transactionid integer NOT NULL
);


ALTER TABLE gamanza.user_transaction OWNER TO ubuntu;

--
-- Name: users; Type: TABLE; Schema: gamanza; Owner: ubuntu
--

CREATE TABLE gamanza.users (
    userid integer NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    balance integer DEFAULT 0 NOT NULL,
    CONSTRAINT users_balance_check CHECK ((balance >= 0))
);


ALTER TABLE gamanza.users OWNER TO ubuntu;

--
-- Name: COLUMN users.balance; Type: COMMENT; Schema: gamanza; Owner: ubuntu
--

COMMENT ON COLUMN gamanza.users.balance IS 'Value represents cents (1/100)';


--
-- Name: user_userid_seq; Type: SEQUENCE; Schema: gamanza; Owner: ubuntu
--

CREATE SEQUENCE gamanza.user_userid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gamanza.user_userid_seq OWNER TO ubuntu;

--
-- Name: user_userid_seq; Type: SEQUENCE OWNED BY; Schema: gamanza; Owner: ubuntu
--

ALTER SEQUENCE gamanza.user_userid_seq OWNED BY gamanza.users.userid;


--
-- Name: transactions transactionid; Type: DEFAULT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.transactions ALTER COLUMN transactionid SET DEFAULT nextval('gamanza.transactions_transactionid_seq'::regclass);


--
-- Name: transfers transferid; Type: DEFAULT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.transfers ALTER COLUMN transferid SET DEFAULT nextval('gamanza.transfers_transferid_seq'::regclass);


--
-- Name: users userid; Type: DEFAULT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.users ALTER COLUMN userid SET DEFAULT nextval('gamanza.user_userid_seq'::regclass);


--
-- Name: transactions transactions_pk; Type: CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.transactions
    ADD CONSTRAINT transactions_pk PRIMARY KEY (transactionid);


--
-- Name: transfers transfers_pk; Type: CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.transfers
    ADD CONSTRAINT transfers_pk PRIMARY KEY (transferid);


--
-- Name: user_transaction user_transaction_pk; Type: CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.user_transaction
    ADD CONSTRAINT user_transaction_pk PRIMARY KEY (userid, transactionid);


--
-- Name: users users_pk; Type: CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.users
    ADD CONSTRAINT users_pk PRIMARY KEY (userid);


--
-- Name: users users_un_email; Type: CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.users
    ADD CONSTRAINT users_un_email UNIQUE (email);


--
-- Name: transfers transfers_fk_receiveid; Type: FK CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.transfers
    ADD CONSTRAINT transfers_fk_receiveid FOREIGN KEY (receiverid) REFERENCES gamanza.users(userid) ON UPDATE CASCADE;


--
-- Name: transfers transfers_fk_senderid; Type: FK CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.transfers
    ADD CONSTRAINT transfers_fk_senderid FOREIGN KEY (senderid) REFERENCES gamanza.users(userid) ON UPDATE CASCADE;


--
-- Name: transfers transfers_fk_transactionid; Type: FK CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.transfers
    ADD CONSTRAINT transfers_fk_transactionid FOREIGN KEY (transactionid) REFERENCES gamanza.transactions(transactionid) ON UPDATE CASCADE;


--
-- Name: user_transaction users_transaction_transactionid; Type: FK CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.user_transaction
    ADD CONSTRAINT users_transaction_transactionid FOREIGN KEY (transactionid) REFERENCES gamanza.transactions(transactionid) ON UPDATE CASCADE;


--
-- Name: user_transaction users_transaction_userid; Type: FK CONSTRAINT; Schema: gamanza; Owner: ubuntu
--

ALTER TABLE ONLY gamanza.user_transaction
    ADD CONSTRAINT users_transaction_userid FOREIGN KEY (userid) REFERENCES gamanza.users(userid) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

