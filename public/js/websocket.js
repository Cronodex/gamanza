$(document).ready(function() {
    var ws = new WebSocket('ws://localhost:8080');
    
    // hide alert messages on x click
    $('.alert').each(function(index, alert) {
        $(alert).find('.close').click(function() {
            $(alert).hide();
        });
    });

    var autoNumericOptions = {
        currencySymbol: ' €',
        currencySymbolPlacement: 's',
        decimalCharacter: ',',
        digitGroupSeparator: '.',
        emptyInputBehavior: 'zero',
        maximumValue: '21474836.47',
        minimumValue: '0'
    };

    var autoNumericBalance = new AutoNumeric('.balance-value', $('.balance-value').data('initial') / 100, autoNumericOptions);

    var autoNumericDeposit = new AutoNumeric('.amount-deposit', 0, autoNumericOptions);
    var autoNumericWithdrawal = new AutoNumeric('.amount-withdrawal', 0, autoNumericOptions);
    var autoNumericTransfer = new AutoNumeric('.amount-transfer', 0, autoNumericOptions);

    // format transaction row so that depending on the transaction type, the amount is colored (green or red) and a - sign is added for negative amounts
    function formatTransaction(transaction) {
        var amount_element = $(transaction).find('.transaction-amount');
        var amount_value = amount_element.data('amount');

        var type_element = $(transaction).find('.transaction-type');
        var type = type_element.text();
        
        if (type === 'deposit') {
            amount_element.addClass('text-success');
        }
        else if (type === 'withdrawal') {
            amount_value *= -1;
            amount_element.addClass('text-danger');
        }
        else if (type === 'transfer') {
            if (type_element.data('sender') !== undefined) {
                amount_element.addClass('text-success');
            }
            else if (type_element.data('receiver') !== undefined) {
                amount_value *= -1;
                amount_element.addClass('text-danger');
            }
        }

        // temporary create an AutoNumeric object for getting the amount formatted
        (new AutoNumeric(amount_element.get(0), amount_value / 100, [autoNumericOptions, { minimumValue: '-21474836.48' }])).remove();

    }

    // iterate through initial transaction rows while formatting them
    $('.transaction').each(function(index, transaction) {
        formatTransaction(transaction);
    });
    
    function disableAction(button) {
        $(button).prop('disabled', true);
    }

    function enableAction(action) {
        $('#' + action).prop('disabled', false);
    }

    // get whole amount in cents as a number from AutoNumeric object assigned to actions (deposit, withdrawal, transfer)
    function getActionAmount(autoNumericObject) {
        return parseInt((autoNumericObject.getNumber()*100).toFixed());
    }

    $('#deposit').click(function() {
        var amount = getActionAmount(autoNumericDeposit);
        if (amount > 0) {
            disableAction(this);
            ws.send(JSON.stringify({
                action: 'deposit',
                amount: amount
            }));
        }
        else {
            $('.amount-deposit').focus();
        }
    });

    $('#withdrawal').click(function() {
        var amount = getActionAmount(autoNumericWithdrawal);
        if (amount > 0) {
            disableAction(this);
            ws.send(JSON.stringify({
                action: 'withdrawal',
                amount: amount
            }));
        }
        else {
            $('.amount-withdrawal').focus();
        }
    });

    $('#transfer').click(function() {
        var email = $('.transfer-email').val();
        if (email === '') {
            $('.transfer-email').focus();
            return;
        }

        var amount = getActionAmount(autoNumericTransfer);
        if (amount > 0) {
            disableAction(this);
            ws.send(JSON.stringify({
                action: 'transfer',
                email: email,
                amount: amount
            }));
        }
        else {
            $('.amount-transfer').focus();
        }
    });

    ws.onopen = function() {
        // immediately send a balance action request to make sure the balance amount displayed is up to date
        ws.send(JSON.stringify({
            action: 'balance' 
        }));
    };

    // check if type specific alert message exists
    function errorTypeExists(type) {
        return $('.error-' + type).length > 0;
    }

    function displayError(type, message) {
        if (typeof message === 'string' && message.length > 0) {
            $('.error-' + type).show().find('.error-text').text(message);
        }
    }

    function hideError(type) {
        $('.error-' + type).hide();
    }

    // set balance to AutoNumeric object which also updates the HTML element
    function setBalance(balance) {
        autoNumericBalance.set(balance / 100);
    }

    // add transaction row to history based on the transaction object returned from server response
    function addTransaction(transaction) {
        if (typeof transaction !== 'object') {
            return;
        }

        var requiredFields = ['transactionid', 'timestamp', 'type', 'source', 'amount', 'sender', 'receiver'];
        for (var i = 0; i < requiredFields.length; i++) {
            if (transaction.hasOwnProperty(requiredFields[i]) === false) {
                return;
            }
        }

        // create transaction row as table row DOM element
        var createTransaction = function() {
            var row = $('<tr></tr>').addClass('transaction').data('id', transaction.transactionid);

            $('<td></td>').text(transaction.timestamp).appendTo(row);

            var type = $('<td></td>').addClass('transaction-type').text(transaction.type).appendTo(row);
            if (transaction.sender !== null) {
                type.data('sender', transaction.sender);
            }
            else if (transaction.receiver !== null) {
                type.data('receiver', transaction.receiver);
            }

            $('<td></td>').text(transaction.source).appendTo(row);

            $('<td></td>').addClass('transaction-amount').data('amount', transaction.amount).text('- €').appendTo(row);

            formatTransaction(row.get(0));

            return row;
        };

        var transaction_rows = $('.transaction');
        if (transaction_rows.length === 0) {
            $('.transactions-empty').remove();
            $('.transactions > tbody').append(createTransaction());
        }
        else {
            // iterate through the current transaction rows and find the right place to insert the row based on the transaction id
            // it is guaranteed by jquery that the order of elements in .each() are the same as in HTML, so the transaction id's are descending
            // iteration is needed for cases when multiple transactions are made at the same time and need to ensure their order are sequentially displayed
            transaction_rows.each(function(index) {
                transactionId = $(this).data('id');
                if (typeof transactionId === 'undefined') {
                    return;
                }
    
                if (transaction.transactionid > transactionId) {
                    $(this).before(createTransaction());
    
                    return false;
                }
            });
        }
    }

    ws.onmessage = function(event) {
        try {
            var response = JSON.parse(event.data);
            switch(response.action) {
                case 'balance':
                    if (response.hasOwnProperty('balance')) {
                        setBalance(response.balance);
                    }
                    else {
                        throw response;
                    }
                    break;
                case 'deposit':
                    enableAction('deposit');

                    if (response.hasOwnProperty('new_balance')) {
                        setBalance(response.new_balance);
                        autoNumericDeposit.set(0);
                        hideError('deposit');
                        addTransaction(response.transaction);
                    }
                    else {
                        throw response;
                    }

                    break;
                case 'withdrawal':
                    enableAction('withdrawal');

                    if (response.hasOwnProperty('new_balance')) {
                        setBalance(response.new_balance);
                        autoNumericWithdrawal.set(0);
                        hideError('withdrawal');
                        addTransaction(response.transaction);
                    }
                    else {
                        throw response;
                    }

                    break;
                case 'transfer':
                    enableAction('transfer');

                    if (response.hasOwnProperty('new_balance')) {
                        setBalance(response.new_balance);
                        autoNumericTransfer.set(0);
                        $('.transfer-email').val('');
                        hideError('transfer');
                        addTransaction(response.transaction);
                    }
                    else {
                        throw response;
                    }

                    break;
                default:
                    if (response.hasOwnProperty('server_error')) {
                        throw response;
                    }
                    else {
                        throw { client_error: 'Unknown server response' };
                    }
                    break;
            }
        }
        catch (error) {
            if (response.hasOwnProperty('server_error')) {
                var type = 'general';

                if (error.action && errorTypeExists(error.action)) {
                    type = error.action;
                }
                
                displayError(type, 'Server error: ' + error.server_error);
            }
            else if (response.hasOwnProperty('client_error')) {
                displayError('general', 'Client error: ' + error.client_error);
            }
            else {
                displayError('general', 'Could not process server response');
            }
        }
    };
});