const http = require('http');
const express = require('express');
const path = require('path');
const expressHandlebars = require('express-handlebars');
const dateFormat = require('dateformat');
const bcrypt = require('bcrypt');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const BasicStrategy = require('passport-http').BasicStrategy;
const flash = require('express-flash');
const session = require('express-session');
const methodOverride = require('method-override');
const postgres = require('./postgres');
const pgp = require('pg-promise')({
    schema: postgres.schema
});
const WebSocket = require('ws');

const app = express();
const server = http.createServer(app);
const db = pgp(postgres.config);
const dateFormat_format = 'd.m.yyyy - H:MM:ss';

// local strategy (username and password) for web application and websocket authentication
passport.use(new LocalStrategy(
    { usernameField: 'email' },
    async function (email, password, done) {
        try {
            const user = await db.oneOrNone('SELECT * FROM users WHERE email = $1', [email]);
            if (user === null) {
                return done(null, false, { message: 'No user with that email' });
            }
            else {
                if (await bcrypt.compare(password, user.password)) {
                    return done(null, user);
                }
                else {
                    return done(null, false, { message: 'Password incorrect' });
                }
            }
        }
        catch (error) {
            return done(error);
        }
    }
));

passport.serializeUser((user, done) => {
    done(null, user.userid);
});
passport.deserializeUser((userid, done) => {
    db.one('SELECT * FROM users WHERE userid = $1', [userid])
    .then(user => {
        done(null, user)
    })
    .catch(error => {
        done(null, false);
    });
});

// basic strategy (username and password) for API authentication
passport.use(new BasicStrategy(
    { usernameField: 'email' },
    async function(email, password, done) {
        try {
            const user = await db.oneOrNone('SELECT * FROM users WHERE email = $1', [email]);
            if (user === null) {
                return done(null, false, { message: 'No user with that email' });
            }
            else {
                if (await bcrypt.compare(password, user.password)) {
                    return done(null, user);
                }
                else {
                    return done(null, false, { message: 'Password incorrect' });
                }
            }
        }
        catch (error) {
            return done(error);
        }
    }
));

handlebars = expressHandlebars.create({
    defaultLayout: 'main',
    extname: '.handlebars',
    helpers: {
        ifeq: function(a, b, options) {
            // compare if both parameters are equal

            if (a == b) {
                return options.fn(this);
            }
            else {
                return options.inverse(this);
            }
        },
        date: function(timestamp, options) {
            // format timestamp to human readable

            return dateFormat(timestamp, dateFormat_format);
        },
        assign_script: function(script, options) {
            // returns HTML script tag with parameter as source file
            // the purpose of this is to be able to assign a script file in the main template from within the inner template

            if (!options.data.root) {
                options.data.root = {};
            }
            options.data.root['template_script'] = `<script src="${script}"></script>`;
        },
        transfer_data: function(transaction, options) {
            // returns the sender or rceiver data HTML attribute for a transaction object

            if (typeof transaction !== 'object') {
                return;
            }

            if (transaction.type === 'transfer') {
                if (typeof transaction.sender !== 'undefined' && transaction.sender !== null) {
                    return `data-sender="${transaction.sender}"`;
                }
                else if (typeof transaction.receiver !== 'undefined' && transaction.receiver !== null) {
                    return `data-receiver="${transaction.receiver}"`;
                }
            }
        }
    }
});

app.engine('handlebars', handlebars.engine);
app.set('render-engine', 'handlebars');
app.set('view engine', '.handlebars');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// save session and passport session objects for later use in websocket authentication
const sessionParser = session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
});
const passportInitialize = passport.initialize();
const passportSession = passport.session();
app.use(sessionParser);
app.use(passportInitialize);
app.use(passportSession);
app.use(methodOverride('_method'));
app.use(flash());
app.use(express.static(path.join(__dirname, '/public')));

app.get('/register', checkNotAuthenticated, (req, res) => {
    res.render('register', { error_message: req.flash('error_register') });
});

app.post('/register', checkNotAuthenticated, (req, res) => {
    db.tx(async t => {
        const exists = await t.oneOrNone('SELECT * FROM users WHERE email = $1', [req.body.email]);
        if (exists === null) {
            const hashedPassword = await bcrypt.hash(req.body.password, 10);
            await t.none('INSERT INTO users(email, password) VALUES($1, $2)', [req.body.email, hashedPassword]);
            return true;
        }
        else {
            return false;
        }
    })
    .then(created => {
        if (created) {
            req.flash('success_login', 'User successfully created');
            res.redirect('/login');
        }
        else {
            req.flash('error_register', `User already exists with email address: ${req.body.email}`);
            res.redirect('/register');
        }
    })
    .catch(error => {
        req.flash('error_register', 'Unexpected error while creating user');
        res.redirect('/register');
    });
});

app.get('/login', checkNotAuthenticated, (req, res) => {
    res.render('login', { success_message: req.flash('success_login') });
});

app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}));

// use method-override for supporting DELETE method in browser
app.delete('/logout', (req, res) => {
    req.logOut();
    res.redirect('/login');
});

app.get('/', checkAuthenticated, async (req, res) => {
    const transactions = await db.manyOrNone(`
        SELECT 
            t.transactionid,
            t.timestamp,
            t.type,
            t.source,
            t.amount,
            (SELECT email FROM users WHERE userid != ut.userid AND userid = tf.senderid) AS sender,
            (SELECT email FROM users WHERE userid != ut.userid AND userid = tf.receiverid) AS receiver
        FROM transactions t
        INNER JOIN user_transaction ut ON ut.transactionid = t.transactionid
        LEFT JOIN transfers tf ON tf.transactionid = t.transactionid
        WHERE ut.userid = $1
        ORDER BY t.transactionid DESC;`,
        [req.user.userid]
    );

    res.render('index', { email: req.user.email, balance: req.user.balance, transactions: transactions });
});

function checkAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }

    res.redirect('/login');
}

function checkNotAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return res.redirect('/');
    }

    next();
}

const checkApiAuthenticated = passport.authenticate('basic', {
    session: false
});

app.get('/api/user/balance', checkApiAuthenticated, async (req, res) => {
    const balance = await action_balance(req.user.userid);
    if (balance.success === true) {
        res.json({ balance: balance.balance });
    }
    else {
        res.json({ error: balance.error });
    }
});

// check for value whether it is a valid positive number
function hasDigitsOnly(value) {
    return /^\d+$/.test(value);
}

function wsSendAll(userid, message) {
    if (userid in wsConnections) {
        wsConnections[userid].forEach(ws => {
            ws.send(message);
        });
    }
}

app.put('/api/user/deposit', checkApiAuthenticated, async (req, res) => {
    const deposit = await action_deposit(req.user.userid, req.body.amount, 'api');
    if (deposit.success === true) {
        res.json({ new_balance: deposit.new_balance });
        wsSendAll(req.user.userid, JSON.stringify({
            action: 'deposit',
            new_balance: deposit.new_balance,
            transaction: deposit.transaction
        }));
    }
    else {
        res.json({ error: deposit.error });
    }
});

app.put('/api/user/withdrawal', checkApiAuthenticated, async (req, res) => {
    const withdrawal = await action_withdrawal(req.user.userid, req.body.amount, 'api');
    if (withdrawal.success === true) {
        res.json({ new_balance: withdrawal.new_balance });
        wsSendAll(req.user.userid, JSON.stringify({
            action: 'withdrawal',
            new_balance: withdrawal.new_balance,
            transaction: withdrawal.transaction
        }));
    }
    else {
        res.json({ error: withdrawal.error });
    }
});

app.put('/api/user/transfer', checkApiAuthenticated, async (req, res) => {
    const transfer = await action_transfer(req.user.userid, req.body.email, req.body.amount, 'api');
    if (transfer.success === true) {
        res.json({ new_balance: transfer.new_balance });
        wsSendAll(req.user.userid, JSON.stringify({
            action: 'transfer',
            new_balance: transfer.new_balance,
            transaction: transfer.transaction_sender
        }));
    }
    else {
        res.json({ error: transfer.error });
    }
});

// saved WebSocket connections for updating balance and transaction history to all applications when using REST API or multiple web applications
const wsConnections = {};

const wss = new WebSocket.Server({ noServer: true });

server.on('upgrade', function(req, socket, head) {
    // check if origin header is matches our site in order to avoid cross site requests
    if (req.headers.origin !== 'http://localhost:8080') {
        socket.destroy();
        return;
    }

    // manually initialize the previously saved session and passport session objects for checking authentication
    sessionParser(req, {}, () => {
        passportInitialize(req, {}, () => {
            passportSession(req, {}, () => {
                if (req.isAuthenticated() === false)  {
                    socket.destroy();
                    return;
                }

                wss.handleUpgrade(req, socket, head, function(ws) {
                    if (req.user.userid in wsConnections === false) {
                        wsConnections[req.user.userid] = [];
                    }
                    wsConnections[req.user.userid].push(ws);

                    wss.emit('connection', ws, req);
                });
            });
        });
    });
});

wss.on('connection', function(ws, req) {
    ws.on('message', async function(message) {
        const response = {};

        try {
            object = JSON.parse(message);
            switch(object.action) {
                case 'balance':
                    response.action = 'balance';

                    const balance = await action_balance(req.user.userid);
                    if (balance.success === true) {
                        response.balance = balance.balance;
                    }
                    else {
                        response.server_error = balance.error;
                    }

                    ws.send(JSON.stringify(response));
                    break;
                case 'deposit':
                        response.action = 'deposit';

                        const deposit = await action_deposit(req.user.userid, object.amount, 'web');
                        if (deposit.success === true) {
                            response.new_balance = deposit.new_balance;
                            response.transaction = deposit.transaction;
                            wsSendAll(req.user.userid, JSON.stringify(response));
                        }
                        else {
                            response.server_error = deposit.error;
                            ws.send(JSON.stringify(response));
                        }
                    break;
                case 'withdrawal':
                        response.action = 'withdrawal';

                        const withdrawal = await action_withdrawal(req.user.userid, object.amount, 'web');
                        if (withdrawal.success === true) {
                            response.new_balance = withdrawal.new_balance;
                            response.transaction = withdrawal.transaction;
                            wsSendAll(req.user.userid, JSON.stringify(response));
                        }
                        else {
                            response.server_error = withdrawal.error;
                            ws.send(JSON.stringify(response));
                        }
                        break;
                case 'transfer':
                        response.action = 'transfer';

                        const transfer = await action_transfer(req.user.userid, object.email, object.amount, 'web');
                        if (transfer.success === true) {
                            response.new_balance = transfer.new_balance;
                            response.transaction = transfer.transaction_sender;
                            wsSendAll(req.user.userid, JSON.stringify(response));
                        }
                        else {
                            response.server_error = transfer.error;
                            ws.send(JSON.stringify(response));
                        }
                        break;
                default:
                    ws.send(JSON.stringify({
                        action: object.action,
                        server_error: `Unknown client action "${object.action}"`
                    }));
                    break;
            }
        }
        catch (error) {
            ws.send(JSON.stringify({
                server_error: 'Could not process client request'
            }));
        }
    });
});

const action_balance = async function(userid) {
    try {
        const user = await db.one('SELECT balance FROM users WHERE userid = $1', [userid]);
        return {
            success: true,
            balance: user.balance
        };
    }
    catch (error) {
        return {
            success: false,
            error: 'Unexpected error while fetching user balance'
        };
    }
};

const action_deposit = async function(userid, amount, channel) {
    if (typeof amount === 'undefined') {
        return {
            success: false,
            error: 'Missing amount value'
        };
    }
    else if (hasDigitsOnly(amount) === false) {
        return {
            success: false,
            error: 'Incorrect amount format'
        };
    }
    
    return await db.tx(async t => {
        const user = await t.one(
            'UPDATE users SET balance=balance+$1 WHERE userid = $2 RETURNING balance AS new_balance',
            [amount, userid]
        );

        const transaction = await t.one(
            'INSERT INTO transactions(amount, type, source) VALUES($1, $2, $3) RETURNING transactionid, timestamp',
            [amount, 'deposit', channel]
        );

        await t.none(
            'INSERT INTO user_transaction(userid, transactionid) VALUES($1, $2)',
            [userid, transaction.transactionid]
        );

        const transaction_log = await action_transaction(userid, transaction.transactionid, t);
        if (transaction_log === null) {
            throw null;
        }

        return {
            new_balance: user.new_balance,
            transaction: transaction_log
        }
    })
    .then(result => {
        return {
            success: true,
            new_balance: result.new_balance,
            transaction: result.transaction
        };
    })
    .catch(error => {
        if (typeof error === 'object' && error.code === '22003') {
            // postgres error code 22003: numeric_value_out_of_range
            return {
                success: false,
                error: 'Cannot go over maximum balance allowed amount'
            };
        }
        else {
            return {
                success: false,
                error: 'Unexpected error while depositing to user balance'
            };
        }
    });
};

const action_withdrawal = async function(userid, amount, channel) {
    if (typeof amount === 'undefined') {
        return {
            success: false,
            error: 'Missing amount value'
        };
    }
    else if (hasDigitsOnly(amount) === false) {
        return {
            success: false,
            error: 'Incorrect amount format'
        };
    }

    return await db.tx(async t => {
        const user = await t.one(
            'UPDATE users SET balance=balance-$1 WHERE userid = $2 RETURNING balance AS new_balance',
            [amount, userid]
        );

        const transaction = await t.one(
            'INSERT INTO transactions(amount, type, source) VALUES($1, $2, $3) RETURNING transactionid',
            [amount, 'withdrawal', channel]
        );

        await t.none(
            'INSERT INTO user_transaction(userid, transactionid) VALUES($1, $2)',
            [userid, transaction.transactionid]
        );

        const transaction_log = await action_transaction(userid, transaction.transactionid, t);
        if (transaction_log === null) {
            throw null;
        }

        return {
            new_balance: user.new_balance,
            transaction: transaction_log
        }
    })
    .then(result => {
        return {
            success: true,
            new_balance: result.new_balance,
            transaction: result.transaction
        };
    })
    .catch(error => {
        if (typeof error === 'object' && error.code === '23514' && error.constraint === 'users_balance_check') {
            // postgres error code 22003: check_violation
            // postgres constraint users_balance_check: custom check if integer is positive
            return {
                success: false,
                error: 'Not enough balance to withdrawal'
            };
        }
        else {
            return {
                success: false,
                error: 'Unexpected error while withdrawing from user balance'
            };
        }
    });
};

const action_transfer = async function(userid, email, amount, channel) {
    const emailExists = async () => {
        const user = await db.oneOrNone('SELECT * FROM users WHERE email = $1', [email]);
        return user !== null;
    };

    if (typeof email === 'undefined') {
        return {
            success: false,
            error: 'Missing email value'
        };
    }
    else if(await emailExists() === false) {
        return {
            success: false,
            error: 'User with given email address doesn\'t exist'
        };
    }
    else if (typeof amount === 'undefined') {
        return {
            success: false,
            error: 'Missing amount value'
        };
    }
    else if (hasDigitsOnly(amount) === false) {
        return {
            success: false,
            error: 'Incorrect amount format'
        };
    }

    return await db.tx(async t => {
        const sender = await t.one(
            'UPDATE users SET balance=balance-$1 WHERE userid = $2 RETURNING balance AS new_balance',
            [amount, userid]
        );

        const receiver = await t.one(
            'UPDATE users SET balance=balance+$1 WHERE email = $2 RETURNING userid', 
            [amount, email]
        );

        const transaction = await t.one(
            'INSERT INTO transactions(amount, type, source) VALUES($1, $2, $3) RETURNING transactionid',
            [amount, 'transfer', channel]
        );

        await t.none(
            'INSERT INTO user_transaction(userid, transactionid) VALUES($1, $2)',
            [userid, transaction.transactionid]
        );

        await t.none(
            'INSERT INTO user_transaction(userid, transactionid) VALUES($1, $2)',
            [receiver.userid, transaction.transactionid]
        );

        await t.none(
            'INSERT INTO transfers(transactionid, senderid, receiverid) VALUES($1, $2, $3)',
            [transaction.transactionid, userid, receiver.userid]
        );

        const transaction_log_sender = await action_transaction(userid, transaction.transactionid, t);
        const transaction_log_receiver = await action_transaction(receiver.userid, transaction.transactionid, t);
        if (transaction_log_sender === null || transaction_log_receiver === null) {
            throw null;
        }

        return {
            new_balance: sender.new_balance,
            transaction_sender: transaction_log_sender,
            transaction_receiver: transaction_log_receiver,
        }
    })
    .then(result => {
        return {
            success: true,
            new_balance: result.new_balance,
            transaction_sender: result.transaction_sender,
            transaction_receiver: result.transaction_log_receiver
        };
    })
    .catch(error => {
        if (typeof error === 'object' && error.code === '23514' && error.constraint === 'users_balance_check') {
            // postgres error code 22003: check_violation
            // postgres constraint users_balance_check: custom check if integer is positive
            return {
                success: false,
                error: 'Not enough balance to transfer'
            };
        }
        else {
            return {
                success: false,
                error: 'Unexpected error while transfering to another user'
            };
        }
    });
}

const action_transaction = async function(userid, transactionid, db) {
    try {
        const transaction = await db.one(`
            SELECT 
                t.transactionid,
                t.timestamp,
                t.type,
                t.source,
                t.amount,
                (SELECT email FROM users WHERE userid != ut.userid AND userid = tf.senderid) AS sender,
                (SELECT email FROM users WHERE userid != ut.userid AND userid = tf.receiverid) AS receiver
            FROM transactions t
            INNER JOIN user_transaction ut ON ut.transactionid = t.transactionid
            LEFT JOIN transfers tf ON tf.transactionid = t.transactionid
            WHERE ut.userid = $1 AND t.transactionid = $2;`,
            [userid, transactionid]
        );
        transaction.timestamp = dateFormat(transaction.timestamp, dateFormat_format);

        return transaction;
    }
    catch (error) {
        return null;
    }
}

server.listen(8080);