## Gamanza wallet application
This is a web wallet application for managing your balance with following functionality:
+ User creation with password authentication
+ Balance actions: deposit, withdrawal and transfer to another user
+ View all transactions history
+ Available REST API for balance actions with basic user authentication
+ Keeps data updated when changes are made from REST API or another opened application

## Installation and usage
Application requires [Node.js](https://nodejs.org/) and [PostgreSQL](https://www.postgresql.org/) to run.

Install Node.js dependencies.
```sh
npm install
```

Create an empty PostgreSQL database and setup the required database schema.
```sh
createdb database
psql database < schema.sql
```

Configure PostgreSQL access and insert essential config data into file `postgres.js`.

Run application. 
```sh
npm start
```
You can access the application by default at `localhost:8080`.

## REST API usage
The application exposes a REST API for managing wallet balance. All amounts are presented and assumed to be in cents. All responses returned are json objects.

### Authorization
Basic Auth is supported and is required to use your email address and password credentials. In case of failed authorization the server returns a 401 http response.

### Errors
In case of an error the application returns a error object.

**Response JSON object**

|Key|Type|Description|
|---|---|---|
|error|string|String message describing the error|

### Balance
Get the balance amount of your user.

**HTTP Request**
`GET /api/user/balance`

**Response JSON object**

|Key|Type|Description|
|---|---|---|
|balance|int|Current balance amount in wallet|

### Deposit
Deposit funds and send to your wallet balance.

**HTTP Request**
`GET /api/user/deposit`

**Request JSON object**

|Key|Type|Description|
|---|---|---|
|amount|int|Amount that we are depositing into wallet|

**Response JSON object**

|Key|Type|Description|
|---|---|---|
|new_balance|int|New balance amount after deposit|

### Withdrawal
Withdrawal funds from your wallet balance.

**HTTP Request**
`GET /api/user/withdrawal`

**Request JSON object**

|Key|Type|Description|
|---|---|---|
|amount|int|Amount that we are withdrawing from wallet|

**Response JSON object**

|Key|Type|Description|
|---|---|---|
|new_balance|int|New balance amount after withdrawal|

### Transfer
Transfer funds from your wallet balance to another user's wallet balance.

**HTTP Request**
`GET /api/user/transfer`

**Request JSON object**

|Key|Type|Description|
|---|---|---|
|email|string|Email address of the receiver|
|amount|int|Amount that we are transfering to receiver|

**Response JSON object**

|Key|Type|Description|
|---|---|---|
|new_balance|int|New balance amount after transfer|
